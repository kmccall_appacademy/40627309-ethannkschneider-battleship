require_relative "player"
require_relative "board"

class BattleshipGame
  attr_reader :board, :player_one

  def initialize(player_one, board)
    # player_one, player_two, board
    @player_one = player_one
    # @player_two = player_two
    @board = board
    @hit = false
  end

  def hit!
    @hit = true
  end

  def attack(pos)
    hit! if @board[pos] == :s
    @board[pos] = :x
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def display_status
    puts "Game Status:"
    puts "Total Ships: #{count}"
    @board.display
  end

  def play_turn
    @hit = false
    display_status
    attack(@player_one.get_play)
    puts "Battleship hit!" if @hit
  end

  def play
    play_turn until game_over?
    puts "Game over!"
  end
end

if __FILE__ == $PROGRAM_NAME
  new_board = Board.random
  player_one = HumanPlayer.new("Ethan")
  game = BattleshipGame.new(player_one, new_board)
  game.play
end
