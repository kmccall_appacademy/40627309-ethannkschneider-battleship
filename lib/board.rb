class Board
  attr_accessor :grid
  attr_reader :number_of_starting_ships

  def initialize(grid = nil)
    grid ? @grid = grid : @grid = Board.default_grid

  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def self.random
    rand_board = Board.new()
    5.times { rand_board.place_random_ship }
    rand_board
  end

  def count
    ship_count = 0
    @grid.each { |row| ship_count += row.count(:s) }

    ship_count
  end

  def [](pos) # e.g. pos = [2, 3]
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def empty?(pos = nil)
    if pos != nil
      return self[pos] == nil ? true : false
    else
      return false if @grid.any? { |row| row.any? { |el| el } }
      return true
    end
  end

  # right now, this will check if the board has zero 'nils'; maybe need to change with bonus section?
  def full?
    @grid.all? { |row| row.all? { |el| el } }
  end

  def place_random_ship
    raise "Board full!" if full?
    try_pos = random_pos

    if empty?(try_pos)
      self[try_pos] = :s
    else
      place_random_ship
    end
  end

  def random_pos
    row, col = rand(0...@grid.length), rand(0...@grid.length)
    [row, col]
  end

  def won?
    return false if @grid.any? { |row| row.any? { |el| el == :s } }
    true
  end

  def display
    (0...@grid.length).each_with_index do |el, index|
      if index == 0
        print "  #{el} "
      else
        print "#{el} "
      end
    end
    puts ""
    @grid.each_with_index do |row, row_index|
      row.each_with_index do |el, col_index|
        temp_mark = ""
        el ? temp_mark = el : temp_mark = " "
        # hide the "S"s when you play the game!
        temp_mark = " " if el == :s
        if col_index == 0
          print "#{row_index} #{temp_mark}"
        else
          print " #{temp_mark}"
        end
      end
      puts ""
    end
  end
end

=begin
if __FILE__ == $PROGRAM_NAME
  new_board = Board.new()
  5.times { new_board.place_random_ship }
  new_board.display
end
=end
