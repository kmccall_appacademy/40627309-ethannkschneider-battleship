class HumanPlayer
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def get_play
    puts "Enter coordinates to attack (e.g. \"1, 2\"):"
    input = gets.chomp
    raise "bad input!" unless input.length == 4
    attack_coordinates = input.split(", ").map(&:to_i)
    attack_coordinates.reverse
  end
end
