class ComputerPlayer
  attr_reader :name
  attr_accessor :board

  def initialize(name)
    @name = name
    @board = nil
  end



  def get_play
    # for now, computer randomly guesses
    # chooses a pos that hasn't been hit yet
    comp_attack = nil
    until @board[comp_attack] != :x
      comp_attack = @board.random_pos
    end
    puts "Computer chose #{comp_attack}"

    comp_attack
  end
end
